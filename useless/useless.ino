/*
Uselessbox par Monsieur Bidouille (Dimitri Ferriere)
Apprendre la programmation arduino en fabriquant son automate simple.
www.fablab-lyon.fr
Tutoriel, plan du cours et documentation de la boite sur :
 */
#include <Servo.h> 
 
Servo myservo;  // créer un objet "myservo" pour controler le servo
int posi = 130; // variable de position du servo
int bouton; // nous créeons une varibale "bouton" de type "int" que nous mettons à 0 par défaut
const int inPin = 3; // lie la variable "inPin" au nombre 3. "const" signifie qu la variable ne peut pas etre modifiée.
void setup() //n'est lu qu'une seule fois

{ 
  Serial.begin(9600); // initialise le port série à 9600 bauds pour communiquer avec le PC (visualisation avec le port série pour débugger le programme)
  myservo.attach(9);  // Lie la pin 9 à l'objet "myservo" (la pin 9 ou est connecté notre servo)
  pinMode(inPin, INPUT_PULLUP); // ici nous précisons que la pin 3 (3 = variable inPin, c'est précisé plus haut) est en mode INPUT, c'est à dire qu'elle reçoit des informations (ici l'état 1 ou 0 de l'interrupteur)
  myservo.write(posi);  // le servo se place en position d'attente
} 
 
void loop() 

{
   bouton = digitalRead(inPin); // ici nous déclarons une autre variable
Serial.println(myservo.read()); // affiche la position du servo sur la console
// eteindre la boite

if (bouton == LOW ) { // Si le bouton est allumé...
  //if (posi >= 130) { delay (random (200, 5000));// délai d'attente aléatoire entre 0,5 et 5 secondes
     for(posi = myservo.read(); posi <=270; ++posi) { // boucle incrémentale entre 70 et 165° 
      myservo.write(posi); // fais tourner le servo de la valeur "posi" incrémentée à chaque tour à la ligne précédente
      delay (1); // délai de 1ms
     }
    Serial.println("ON"); // envoyer "ON" sur la console
    } 
else {
    // retour en position et rien faire
    for(posi = myservo.read(); posi >=130; --posi) {
      myservo.write(posi);
      delay (5);
    }
      Serial.println("OFF"); // le serial envoie "off" pour vérifier son état
}

    
}
